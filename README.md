# Usage

In the Puppetfile use:

```
mod 'nli_fw',

  :git => 'https://bitbucket.org/nlireland/puppet_module-nli_fw',
```

Override ` nli_fw::firewall_active_ports_http: "[PORT NUMBER]" ` in the node specific yaml file in hieradata/node/hostname.yaml


Override `nli_fw::firewall_rules: ` in the node specific yaml file in hieradata/node/hostname.yaml, the structure should be:

```
nli_fw::firewall_rules:
  100 allow http and https access:
    source: 172.16.100.1/32
  101 allow http and https access:
    source: 172.16.100.2/32
  102 allow http and https access:
    source: 172.16.1.100/32

```

----

#### Testing

This module has spec files which can be run using `bundle exec rake spec`.
It also has a linter which can be run with `bundle exec puppet-lint .` (note the dot at the end which runs for current directory and all sub-directories)
