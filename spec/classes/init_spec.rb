# frozen_string_literal: true

require 'spec_helper'

describe 'nli_fw' do
  on_supported_os.each do |os, os_facts|
    let(:facts) { os_facts }

    context "where OS is #{os}" do
      it { is_expected.to compile.with_all_deps }

      it { is_expected.to contain_class('firewall') }
      it { is_expected.to contain_class('nli_fw::pre') }
      it { is_expected.to contain_class('nli_fw::post') }
    end
  end
end
