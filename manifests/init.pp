# Wrapper module for Puppet's Firewall module, containing NLI specific setup
class nli_fw(
  $firewall_active_ports_http = $nli_fw::params::firewall_active_ports_http,
  $firewall_rules = $nli_fw::params::firewall_rules,
  ) inherits nli_fw::params {

  resources { 'firewall':
    purge => true
  }

  Firewall {
    before => Class['nli_fw::post'],
    require => Class['nli_fw::pre'],
  }

  class { ['nli_fw::pre', 'nli_fw::post']: }
  class { 'firewall': }

  if $firewall_rules != [] {
    $defaults = {
        dport   => $firewall_active_ports_http,
        proto  => tcp,
        action => accept,
    }
    create_resources(firewall, $firewall_rules, $defaults)
  } else {
    # There are no specific source addresses to whitelist,
    # Allow access from everywhere
    firewall { '100 allow http and https access':
      dport  => $firewall_active_ports_http,
      proto  => tcp,
      action => accept,
    }
  }
}



