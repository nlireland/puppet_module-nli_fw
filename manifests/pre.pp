# Rules that the firewall applies when a service requests access. It is run before any other rules
class nli_fw::pre {
  Firewall { require => undef, }

  # Default firewall rules
  firewall {'000 accept all icmp':
    proto  => 'icmp',
    action => 'accept',
  }
  -> firewall { '001 accept all to lo interface':
    proto   => 'all',
    iniface => 'lo',
    action  => 'accept',
  }
  -> firewall { '002 accept related established rules':
    proto  => 'all',
    state  => ['RELATED', 'ESTABLISHED'],
    action => 'accept',
  }
  firewall { '010 allow ssh from Network':
    dport    => '22',
    proto    => 'tcp',
    source   => '172.16.0.0/16',
    action   => 'accept',
    provider => 'iptables',
  }
}
