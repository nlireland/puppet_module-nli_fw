# The post rules tell the firewall to drop requests that haven’t met the rules defined by pre.pp or in site.pp
class nli_fw::post {
  firewall { '999 drop all':
    proto  => 'all',
    action => 'drop',
    before => undef,
  }
}
