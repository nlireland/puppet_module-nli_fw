# Module data. Don't put business specific or sensitive data here. Use hiera instead.
class nli_fw::params {
  $firewall_active_ports_http = []
  $firewall_rules = []
}
